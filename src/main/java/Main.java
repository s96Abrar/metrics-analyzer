import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Main {

    public static String FILE_SEPARATOR = System.getProperty("file.separator");
    public static String LINE_SEPARATOR = System.getProperty("line.separator");

    /**
     * Main Method
     *
     * @param args program arguments
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Please provide a source file or a project folder as program argument.");
            return;
        }

        String today = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss").format(LocalDateTime.now());

        List<SourceFile> inputFiles = new ArrayList<>();

        getListOfFiles(args, inputFiles);
        Collections.sort(inputFiles);

        String header = "No,File Name,Line of Code,Commented Line of Code,Non Commented Line of Code," +
                "Number of Blank Lines,Density of Comments,Number of Characters,Number of Characters per line," +
                "Halsteads Distinct Operator Count,Halsteads Distinct Operand Count," +
                "Halsteads Total Operator Count,Halsteads Total Operand Count," +
                "Halsteads Program Volume,Halsteads Program Difficulty,Halsteads Program Effort," +
                "Class Count,Overloaded Method Count,Public Method Count,Private Method Count," +
                "Public Static Method Count,Private Static Method Count,Total Static Method Count," +
                "Total Method Count";

        Map<String, Integer> distinctOperator = new HashMap<>();
        Map<String, Integer> distinctOperand = new HashMap<>();

        try {
            long totalClassCount = 0;
            long totalLineOfCode = 0;
            long totalCommentedLineOfCode = 0;
            long totalNonCommentedLineOfCode = 0;
            long totalBlankLines = 0;
            double totalDensityOfComments = 0;
            long totalCharacterCount = 0;
            double totalCharactersPerLine = 0;
            long totalOperatorCount = 0;
            long totalOperandCount = 0;
            long totalDistinctOperator;
            long totalDistinctOperand;
            double programVolume = 0;
            double programDifficulty = 0;
            double programEffort = 0;
            long totalOverloadedMethodCount = 0;
            long totalPublicMethodCount = 0;
            long totalPrivateMethodCount = 0;
            long totalPublicStaticMethodCount = 0;
            long totalPrivateStaticMethodCount = 0;
            long totalStaticMethodCount = 0;
            long totalMethodCount = 0;

            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("Metrics-" + today + ".csv"));
            bufferedWriter.append(header);
            bufferedWriter.newLine();

            int count = 1;
            for (SourceFile f : inputFiles) {
                System.out.println(f.getFilepath());
                f.generateMetrics();
                f.printMetrics();

                totalClassCount += f.getClassCount();
                totalLineOfCode += f.getLineOfCode();
                totalCommentedLineOfCode += f.getCommentedLineOfCode();
                totalNonCommentedLineOfCode += f.getNonCommentedLineOfCode();
                totalBlankLines += f.getBlankLines();
                totalDensityOfComments += f.getDensityOfComments();
                totalCharacterCount += f.getCharacterCount();
                totalCharactersPerLine += f.getAverageNoOfCharPerLine();
                totalOperandCount += f.getTotalOperand();
                totalOperatorCount += f.getTotalOperator();
                totalOverloadedMethodCount += f.getOverloadedMethodCount();
                totalPublicMethodCount += f.getPublicMethodCount();
                totalPrivateMethodCount += f.getPrivateMethodCount();
                totalPublicStaticMethodCount += f.getPublicStaticMethodCount();
                totalPrivateStaticMethodCount += f.getPrivateStaticMethodCount();
                totalStaticMethodCount += f.getTotalStaticMethodCount();
                totalMethodCount += f.getTotalMethodCount();

                bufferedWriter.append(
                        String.format("%d,%s,%d,%d,%d,%d,%.2f,%d,%.2f,%d,%d,%d,%d,%.2f,%.2f,%.2f,%d,%d,%d,%d,%d,%d,%d,%d",
                                count, f.getFileName(), f.getLineOfCode(), f.getCommentedLineOfCode(), f.getNonCommentedLineOfCode(),
                                f.getBlankLines(), f.getDensityOfComments(), f.getCharacterCount(), f.getAverageNoOfCharPerLine(),
                                f.getDistinctOperator(), f.getDistinctOperand(), f.getTotalOperator(), f.getTotalOperand(),
                                f.getProgramVolume(), f.getProgramDifficulty(), f.getProgramEffort(), f.getClassCount(),
                                f.getOverloadedMethodCount(), f.getPublicMethodCount(), f.getPrivateMethodCount(),
                                f.getPublicStaticMethodCount(), f.getPrivateStaticMethodCount(), f.getTotalStaticMethodCount(),
                                f.getTotalMethodCount()
                        )
                );
                bufferedWriter.newLine();

                distinctOperator.putAll(f.getOperators());
                distinctOperand.putAll(f.getOperands());

                count++;
            }

            if (totalClassCount != 0) {
                totalDensityOfComments = totalDensityOfComments / totalClassCount;
            }
            if (totalClassCount != 0) {
                totalCharactersPerLine = totalCharactersPerLine / totalClassCount;
            }
            totalDistinctOperator = distinctOperator.size();
            totalDistinctOperand = distinctOperand.size();
            if (totalDistinctOperator != 0 && totalDistinctOperand != 0) {
                programVolume = (1.0 * totalOperandCount + totalOperatorCount) * Math.log(1.0 * (totalDistinctOperand + totalDistinctOperator));
                programDifficulty = ((1.0 * totalDistinctOperator / 2) * (1.0 * totalOperandCount / totalDistinctOperand));
                programEffort = programVolume * programDifficulty;
            }

            bufferedWriter.append(
                    String.format(",%s,%d,%d,%d,%d,%.2f,%d,%.2f,%d,%d,%d,%d,%.2f,%.2f,%.2f,%d,%d,%d,%d,%d,%d,%d,%d",
                            "Total", totalLineOfCode, totalCommentedLineOfCode, totalNonCommentedLineOfCode,
                            totalBlankLines, totalDensityOfComments, totalCharacterCount, totalCharactersPerLine,
                            totalDistinctOperator, totalDistinctOperand, totalOperatorCount, totalOperandCount,
                            programVolume, programDifficulty, programEffort, totalClassCount,
                            totalOverloadedMethodCount, totalPublicMethodCount, totalPrivateMethodCount,
                            totalPublicStaticMethodCount, totalPrivateStaticMethodCount, totalStaticMethodCount,
                            totalMethodCount
                    )
            );

            bufferedWriter.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println("\nDistinct Operator: " + distinctOperator.size());
        System.out.println("Distinct Operand: " + distinctOperand.size());
    }

    // File utilities
    private static void getListOfFiles(String[] args, List<SourceFile> inputFiles) {
        for (String filepath : args) {
            if (!filepath.endsWith(FILE_SEPARATOR)) {
                filepath += FILE_SEPARATOR;
            }

            System.out.println("Entering '" + filepath + "'");

            try {
                inputFiles.addAll(getFilesRecursively(new File(filepath)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static List<SourceFile> getFilesRecursively(File file) throws IOException {
        List<SourceFile> files = new ArrayList<>();

//        if (file.isDirectory()) {
            File[] listOfFiles = file.listFiles();
            if (listOfFiles != null) {
                for (File f : listOfFiles) {
                    if (f.isDirectory()) {
                        files.addAll(getFilesRecursively(f.getCanonicalFile()));
                    }

                    addFileToFiles(f, files);
                }
            }
//        }

        addFileToFiles(file, files);

        return files;
    }

    private static void addFileToFiles(File file, List<SourceFile> files) throws IOException {
        if (file.isFile() && file.getName().toLowerCase().endsWith(".java")) {
            files.add(new SourceFile(file.getCanonicalPath()));
        }
    }

}
