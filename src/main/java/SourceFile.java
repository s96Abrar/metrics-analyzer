import external.ASTVisitorMod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* FIXME: Can not address inner class as separate class inside one java file */
/* FIXME: Can not detect single comment inside multi comment */
/* FIXME: Can not address constructor like `<constructor-name>(...) { ... }` */
/* FIXME: Can not address inline class like `addClass(new Clazz() { ... }` */
/* FIXME: Can not address method or class which are written in multiline */

/*
 * CAUTION: Commented line of code address any comment in a line
 * e.g. `int test; // This is considered as a commented line`
 */

public class SourceFile implements Comparable<SourceFile> {
    private final String filepath;

    private long lineOfCode; /* FIXME: Cannot address the last blank line */
    private long blankLines;
    private int commentedLineOfCode;
    private long nonCommentedLineOfCode;
    private double densityOfComments;
    private double averageNoOfCharPerLine;
    private long characterCount;
//    private int stringOperandsCount;

    private long distinctOperator;
    private long distinctOperand;
    private long totalOperator;
    private long totalOperand;
    private double programVolume;
    private double programDifficulty;
    private double programEffort;

    private int classCount;

    private StringBuilder programCode;
    private StringBuilder noCommentNoStringProgramCode;
    private StringBuilder noPreprocessorNoCommentProgramCode;
    private StringBuilder noStringOperandProgramCode;

    private Map<String, Integer> operators;
    private Map<String, Integer> operands;
    private Map<String, Integer> preprocessorUsed;
    private Map<String, ClassStat> classes;
    private List<String> classList;

    public Map<String, Integer> getOperators() {
        return operators;
    }

    public Map<String, Integer> getOperands() {
        return operands;
    }

    public String getFileName() {
        return new File(filepath).getName();
    }

    public int getClassCount() {
        return classCount;
    }

    private List<String> methods;
    private Map<String, Integer> overloadedMethod;

    private int overloadedMethodCount;
    private int publicMethodCount;
    private int privateMethodCount;
    private int totalStaticMethodCount;
    private int publicStaticMethodCount;
    private int privateStaticMethodCount;
    private int totalMethodCount;

    @Override
    public int compareTo(SourceFile sourceFile) {
        return this.getFileName().toLowerCase().compareTo(sourceFile.getFileName().toLowerCase());
    }

    public class ClassStat {
        private final String className;
        private String classCode;

        public ClassStat(String className, String classCode) {
            reset();
            this.className = className;
            this.classCode = classCode;
            getMethodList(classCode);
            calculateDesignSize();
        }

        private void calculateDesignSize() {
            totalMethodCount = methods.size();

            for (int count : overloadedMethod.values()) {
                if (count > 1) {
                    overloadedMethodCount++;
                }
            }

            for (String method : methods) {
                int start = 0;
                int methodType = 0;
                if (method.startsWith("public")) {
                    publicMethodCount++;
                    start = 6;
                    methodType = 1;
                } else if (!method.startsWith("abstract")) { // Count all non public and non abstract method as private method
                    privateMethodCount++;
                    start = 7;
                    methodType = 2;
                }

                if (method.substring(start).trim().startsWith("static")) {
                    if (methodType == 1) {
                        publicStaticMethodCount++;
                    } else if (methodType == 2) {
                        privateStaticMethodCount++;
                    }
                    totalStaticMethodCount++;
                }
            }
        }

        private void getMethodList(String code) {
            if (code.trim().isEmpty()) {
                return;
            }

            String methodCapturePattern = "(public|private|abstract)*\\w+([\\[\\]<>, \\w]*) +[\\w_]+ *\\([^)]*\\) *[\\w ]*\\{";
            //"(public|private|abstract)+\\s+\\w+([\\[\\]<>, \\w]*) +[\\w_]+ *\\([^)]*\\) *[\\w ]*\\{";
            Pattern methodCapture = Pattern.compile(methodCapturePattern);
            Matcher matcher = methodCapture.matcher(code);

            while (matcher.find()) {
                String method = code.substring(matcher.start(), matcher.end());
                if (!method.trim().isEmpty()) {
                    methods.add(method.trim());
                }
            }

            for (String method : methods) {
                String methodSignature = getMethodName(method);
                if (!methodSignature.isEmpty()) {
                    addMethod(methodSignature.trim());
                }
            }
        }

        private String getMethodName(String method) {
            String methodName = "";
            if (!method.isEmpty()) {
                String captureFromMethodPattern = "[\\w_]+(?= *\\([^)]*\\) *[\\w ]*\\{)";
                Pattern pattern = Pattern.compile(captureFromMethodPattern);
                Matcher matcher = pattern.matcher(method);

                while (matcher.find()) {
                    methodName = method.substring(matcher.start(), matcher.end());
                }
            }
            return methodName;
        }

        private void addMethod(String method) {
            addToMap(method, overloadedMethod);
        }

        public void reset() {
            classCode = "";

            methods = new ArrayList<>();
            overloadedMethod = new HashMap<>();

            overloadedMethodCount = 0;
            publicMethodCount = 0;
            privateMethodCount = 0;
            totalStaticMethodCount = 0;
            publicStaticMethodCount = 0;
            privateStaticMethodCount = 0;
            totalMethodCount = 0;
        }

        public String getClassName() {
            return className;
        }
    }

    public SourceFile(String filepath) {
        this.filepath = filepath;
        resetMetrics();
    }

    public String getFilepath() {
        return filepath;
    }

    private void addToMap(String key, Map<String, Integer> map) {
        if (map.containsKey(key)) {
            map.put(key, map.get(key) + 1);
        } else {
            map.put(key, 1);
        }
    }

    private void addPreprocessor(String preprocessor) {
        addToMap(preprocessor, preprocessorUsed);
    }

    private void addOperator(String operator) {
        addToMap(operator, operators);
    }

    private void addOperand(String operand) {
        addToMap(operand, operands);
    }

    private void resetMetrics() {
        lineOfCode = 0;
        blankLines = 0;
        commentedLineOfCode = 0;
        nonCommentedLineOfCode = 0;
        averageNoOfCharPerLine = 0;
        densityOfComments = 0;
        characterCount = 0;
//        stringOperandsCount = 0;

        distinctOperator = 0;
        distinctOperand = 0;
        totalOperator = 0;
        totalOperand = 0;
        programVolume = 0.0;
        programDifficulty = 0.0;
        programEffort = 0.0;

        classCount = 0;

        operators = new HashMap<>();
        operands = new HashMap<>();
        preprocessorUsed = new HashMap<>();
        programCode = new StringBuilder();
        noCommentNoStringProgramCode = new StringBuilder();
        noPreprocessorNoCommentProgramCode = new StringBuilder();
        noStringOperandProgramCode = new StringBuilder();

        classes = new HashMap<>();
        classList = new ArrayList<>();
    }

    /**
     * Removing the extra text that collected from main program text.
     */
    private void cleanResiduals() {
        /*
//        operators = new HashMap<>();
//        operands = new HashMap<>();
         */
        preprocessorUsed = new HashMap<>();

        programCode = new StringBuilder();
        noCommentNoStringProgramCode = new StringBuilder();
        noPreprocessorNoCommentProgramCode = new StringBuilder();
        noStringOperandProgramCode = new StringBuilder();

        classes = new HashMap<>();
    }

    public void printMetrics() {
        System.out.println("LOC: " + lineOfCode);
        System.out.println("CLOC: " + commentedLineOfCode);
        System.out.println("NCLOC: " + nonCommentedLineOfCode);
        System.out.println("Blank lines: " + blankLines);
        System.out.println("Density of comments: " + String.format("%.2f", densityOfComments) + "%");
        System.out.println("Character Count: " + characterCount);
        System.out.println("Average number of characters per line: " + String.format("%.2f", averageNoOfCharPerLine));

        System.out.println("*** Halstead Metrics ***");
        System.out.println("Unique operator(μ1): " + distinctOperator);
        System.out.println("Unique operand(μ2): " + distinctOperand);
        System.out.println("Total occurrence of operator(N1): " + totalOperator);
        System.out.println("Total occurrence of operand(N2): " + totalOperand);
        System.out.println("Program vocabulary(μ): " + (distinctOperand + distinctOperator));
        System.out.println("Program length(N): " + (totalOperand + totalOperator));
        System.out.println("Program volume: " + programVolume);
        System.out.println("Program difficulty: " + programDifficulty);
        System.out.println("Effort: " + programEffort);

        for (ClassStat c : classes.values()) {
            System.out.println("Class: " + c.getClassName());
            System.out.println("Overloaded method count: " + overloadedMethodCount);
            System.out.println("Public method count: " + publicMethodCount);
            System.out.println("Private method count: " + privateMethodCount);
            System.out.println("Public static method count: " + publicStaticMethodCount);
            System.out.println("Private static method count: " + privateStaticMethodCount);
            System.out.println("Total static method count: " + totalStaticMethodCount);
            System.out.println("Total method count: " + totalMethodCount);
        }
    }

    private void readFile() throws IOException {
        FileReader fileReader = new FileReader(filepath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;

        while ((line = bufferedReader.readLine()) != null) {
            if (line.trim().isEmpty()) {
                this.blankLines++;
            }
            this.programCode.append(line).append(Main.LINE_SEPARATOR);
            this.characterCount += line.length();
            this.lineOfCode++;
        }
        bufferedReader.close();
    }

    public void generateMetrics() throws IOException {
        cleanResiduals();
        readFile();

        this.noStringOperandProgramCode = removeStringOperands(this.programCode);
        this.noCommentNoStringProgramCode = removeCommentedCode(this.noStringOperandProgramCode);
        this.noPreprocessorNoCommentProgramCode = removePreprocessorCode(this.noCommentNoStringProgramCode);

        this.characterCount += this.lineOfCode; /* Appending the \n character */
        this.nonCommentedLineOfCode = this.lineOfCode - this.blankLines - this.commentedLineOfCode;
        this.densityOfComments = 100.0 * commentedLineOfCode / (nonCommentedLineOfCode + commentedLineOfCode);
        this.averageNoOfCharPerLine = 1.0 * characterCount / lineOfCode;

        getOperatorsFromCode(noPreprocessorNoCommentProgramCode);
        getOperandsAndKeywords(noPreprocessorNoCommentProgramCode);

        calculateHalsteadMetrics();

        getClassAndGenerateDesignMetrics(noPreprocessorNoCommentProgramCode);

        cleanResiduals();
    }

    private void getClassAndGenerateDesignMetrics(StringBuilder code) {
        if (code.toString().trim().isEmpty()) return;
        String classPattern = "(((public|private|abstract|public abstract|private abstract)?\\s+class\\s+)\\w+.*[\\n]*\\{)";
        Pattern pattern = Pattern.compile(classPattern);
        Matcher matcher = pattern.matcher(code.toString());

        classList = new ArrayList<>();

        while (matcher.find()) {
            String classSignature = code.substring(matcher.start(), matcher.end()).trim();
            classList.add(getClassName(classSignature));
        }

        for (String className : classList) {
            this.classes.put(className, new ClassStat(className, code.toString()));
        }

        this.classCount = classList.size();
    }

    private String getClassName(String classSignature) {
        String className = "";
        if (!classSignature.isEmpty()) {
            String classNamePattern = "(?!(public|private|abstract|public abstract|private abstract))\\s+class\\s+\\w+(?=.*\\{)";
            Pattern pattern = Pattern.compile(classNamePattern);
            Matcher matcher = pattern.matcher(classSignature);

            while (matcher.find()) {
                className = classSignature.substring(matcher.start(), matcher.end()).trim().substring(5).trim();
            }
        }
        return className;
    }

    private void calculateHalsteadMetrics() {
        this.distinctOperator = this.operators.size();
        this.distinctOperand = this.operands.size();
        this.totalOperator = 0;
        this.totalOperand = 0;

        for (int count : operators.values()) {
            this.totalOperator += count;
        }

        for (int count : operands.values()) {
            this.totalOperand += count;
        }

        this.programVolume = (totalOperand + totalOperator) * Math.log(1.0 * (distinctOperand + distinctOperator));
        this.programDifficulty = ((1.0 * distinctOperator / 2) * (1.0 * totalOperand / distinctOperand));
        this.programEffort = this.programVolume * this.programDifficulty;
    }

    private void getOperandsAndKeywords(StringBuilder code) {
        String wordsAndIdentifierPattern = "\\b_*[a-zA-Z][_a-zA-Z0-9]*\\b";
        Pattern pattern = Pattern.compile(wordsAndIdentifierPattern);
        Matcher matcher = pattern.matcher(code.toString());

        while (matcher.find()) {
            String identifier = code.substring(matcher.start(), matcher.end());
            if (keywordsAndPrimitiveDataTypes.contains(identifier)) {
                addOperator(identifier);
            } else {
                addOperand(identifier);
            }
        }
    }

    private void getOperatorsFromCode(StringBuilder code) {
        if (code.toString().trim().isEmpty()) return;
        String operatorPattern = "(>>>=|>>=|<<=|->|==|>=|<=|!=|&&|\\|\\||\\+\\+|--|<<|>>|>>>|\\+=|-=|\\*=|/=|&=|\\|=|\\^=|%=|=|>|<|!|~|\\?|\\+|-|\\*|/|&|\\||\\^|%)";
        Pattern pattern = Pattern.compile(operatorPattern);
        Matcher matcher = pattern.matcher(code.toString());

        while (matcher.find()) {
            String operator = code.substring(matcher.start(), matcher.end());
            addOperator(operator);
        }
    }

    private StringBuilder removePreprocessorCode(StringBuilder code) {
        if (code.toString().trim().isEmpty()) {
            return new StringBuilder();
        }

        StringBuilder newCode = new StringBuilder();
        String preprocessorPattern = "(import[a-zA-Z .]+;|package[a-zA-Z .]+;)";

        Pattern pattern = Pattern.compile(preprocessorPattern);
        Matcher matcher = pattern.matcher(code);

        int prev = 0;
        while (matcher.find()) {
            String preprocessor = code.substring(matcher.start(), matcher.end());
            if (preprocessor.startsWith("import")) {
                addOperator("import");
            } else if (preprocessor.startsWith("package")) {
                addOperator("package");
            }
            addPreprocessor(preprocessor);
            newCode.append(code, prev, matcher.start());
            prev = matcher.end();
        }
        newCode.append(code, prev, code.length());

        return newCode;
    }

    private StringBuilder removeCommentedCode(StringBuilder code) {
        if (code.toString().trim().isEmpty()) {
            return new StringBuilder();
        }

        StringBuilder newNonCommentedCode = new StringBuilder();

        this.commentedLineOfCode = 0;
        String multiLineCommentPattern = "(/\\*(.|[\\r\\n])*?\\*/|//.*)";

        Pattern pattern = Pattern.compile(multiLineCommentPattern);
        Matcher matcher = pattern.matcher(code);

        int prev = 0;
        while (matcher.find()) {
            String comment = code.substring(matcher.start(), matcher.end());
            this.commentedLineOfCode += comment.split("\n").length;

            newNonCommentedCode.append(code, prev, matcher.start());
            prev = matcher.end();
        }
        newNonCommentedCode.append(code, prev, code.length());

        return newNonCommentedCode;
    }

    private StringBuilder removeStringOperands(StringBuilder code) {
        if (code.toString().trim().isEmpty()) {
            return new StringBuilder();
        }

//        this.stringOperandsCount = 0;

        StringBuilder newCode = new StringBuilder();

        String stringPattern = "\"[^\"\\\\]*(\\\\(.|\\n)[^\"\\\\]*)*\"|'[^'\\\\]*(\\\\(.|\\n)[^'\\\\]*)*'";
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(code);

        int prev = 0;
        while (matcher.find()) {
            addOperand(code.substring(matcher.start(), matcher.end()));

            newCode.append(code, prev, matcher.start());
            prev = matcher.end();
//            stringOperandsCount++;
        }
        newCode.append(code, prev, code.length());
        return newCode;
    }

    private void halsteadMetricsASTVisitorMod(String code) {
        long distinctOperator;
        long distinctOperand;
        long totalOperator = 0;
        long totalOperand = 0;

        char[] fileCharArray = code.toCharArray();
        ASTVisitorMod astVisitorModFile = ASTVisitorMod.parse(fileCharArray);

        for (int count : astVisitorModFile.oprt.values()) {
            totalOperator += count;
        }

        for (int count : astVisitorModFile.names.values()) {
            totalOperand += count;
        }

        distinctOperator = astVisitorModFile.oprt.size();
        distinctOperand = astVisitorModFile.names.size();

        double volume = (totalOperand + totalOperator) * Math.log(1.0 * (distinctOperand + distinctOperator));

        System.out.println("*** Halstead's Metrics ***");
        System.out.println("Unique operator(μ1): " + distinctOperator);
        System.out.println("Unique operand(μ2): " + distinctOperand);
        System.out.println("Total occurrence of operator(N1): " + totalOperator);
        System.out.println("Total occurrence of operand(N2): " + totalOperand);
        System.out.println("Program vocabulary(μ): " + (distinctOperand + distinctOperator));
        System.out.println("Program length(N): " + (totalOperand + totalOperator));
        System.out.println("Program volume: " + volume);
    }

    public long getLineOfCode() {
        return lineOfCode;
    }

    public long getBlankLines() {
        return blankLines;
    }

    public int getCommentedLineOfCode() {
        return commentedLineOfCode;
    }

    public long getNonCommentedLineOfCode() {
        return nonCommentedLineOfCode;
    }

    public double getDensityOfComments() {
        return densityOfComments;
    }

    public double getAverageNoOfCharPerLine() {
        return averageNoOfCharPerLine;
    }

    public long getCharacterCount() {
        return characterCount;
    }

    public long getDistinctOperand() {
        return distinctOperand;
    }

    public long getDistinctOperator() {
        return distinctOperator;
    }

    public long getTotalOperand() {
        return totalOperand;
    }

    public long getTotalOperator() {
        return totalOperator;
    }

    public double getProgramVolume() {
        return programVolume;
    }

    public double getProgramDifficulty() {
        return programDifficulty;
    }

    public double getProgramEffort() {
        return programEffort;
    }

    public int getOverloadedMethodCount() {
        return overloadedMethodCount;
    }

    public int getPublicMethodCount() {
        return publicMethodCount;
    }

    public int getPrivateMethodCount() {
        return privateMethodCount;
    }

    public int getPublicStaticMethodCount() {
        return publicStaticMethodCount;
    }

    public int getPrivateStaticMethodCount() {
        return privateStaticMethodCount;
    }

    public int getTotalStaticMethodCount() {
        return totalStaticMethodCount;
    }

    public int getTotalMethodCount() {
        return totalMethodCount;
    }

    private final List<String> keywordsAndPrimitiveDataTypes = new ArrayList<>() {
        {
            addAll(Arrays.asList(
                    "abstract", "continue", "for", "new", "switch", "assert", "default", "package", "synchronized", "do",
                    "boolean", "if", "private", "this", "break", "double", "implements", "protected", "throw", "byte", "else",
                    "import", "public", "throws", "case", "enum", "instanceof", "return", "transient", "catch", "extends",
                    "int", "short", "try", "char", "final", "interface", "static", "void", "class", "finally", "long", "volatile",
                    "float", "native", "super", "while"
            ));
        }
    };
}
