/*
  This file is collected from https://github.com/aametwally/Halstead-Complexity-Measures
  Suitable modifications has been made to work with this program
  @author Ahmed Metwally
 */

package external;

import java.util.HashMap;

import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.core.dom.*;


// This class is intended to override the specific methods in the ASTVisitor in order to
// calculate the operator and operands and
public class ASTVisitorMod extends ASTVisitor {
    public HashMap<String, Integer> names = new HashMap<>();
    public HashMap<String, Integer> oprt = new HashMap<>();
    public HashMap<String, Integer> declaration = new HashMap<>();
    CompilationUnit compilation = null;

    public static ASTVisitorMod parse(char[] str) {
        ASTParser parser = ASTParser.newParser(AST.JLS17);
        parser.setSource(str);
        parser.setKind(ASTParser.K_COMPILATION_UNIT);
        parser.setResolveBindings(true);
        final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

        // Check for compilationUnits problems in the provided file
        IProblem[] problems = cu.getProblems();
        for (IProblem problem : problems) {
            // Ignore some error because of the different versions.
            if (problem.getID() == 1610613332)         // 1610613332 = Syntax error, annotations are only available if source level is 5.0
                continue;
            else if (problem.getID() == 1610613329) // 1610613329 = Syntax error, parameterized types are only available if source level is 5.0
                continue;
            else if (problem.getID() == 1610613328) // 1610613328 = Syntax error, 'for each' statements are only available if source level is 5.0
                continue;
            else {
                // quit compilation if
                System.out.println("CompilationUnit problem Message " + problem.getMessage() + " \t At line= " + problem.getSourceLineNumber() + "\t Problem ID=" + problem.getID());

                System.out.println("The program will quit now!");
                System.exit(1);
            }
        }

        // visit nodes of the constructed AST
        ASTVisitorMod visitor = new ASTVisitorMod();
        cu.accept(visitor);

        return visitor;
    }

    // Override visit the infix expressions nodes.
    // if the expression's operator doesn't exist in the operator hashmap, insert it, otherwise, increment the count field.
    public boolean visit(InfixExpression node) {
        if (!this.oprt.containsKey(node.getOperator().toString())) {
            this.oprt.put(node.getOperator().toString(), 1);
        } else {
            this.oprt.put(node.getOperator().toString(), this.oprt.get(node.getOperator().toString()) + 1);
        }
        return true;
    }


    // Override visit the postfix expressions nodes.
    // if the expression's operator doesn't exist in the operator hashmap, insert it, otherwise, increment the count field.
    public boolean visit(PostfixExpression node) {
        if (!this.oprt.containsKey(node.getOperator().toString())) {
            this.oprt.put(node.getOperator().toString(), 1);
        } else {
            this.oprt.put(node.getOperator().toString(), this.oprt.get(node.getOperator().toString()) + 1);
        }
        return true;
    }


    // Override visit the prefix expressions nodes.
    // if the expression's operator doesn't exist in the operator hashmap, insert it, otherwise, increment the count field.
    public boolean visit(PrefixExpression node) {
        if (!this.oprt.containsKey(node.getOperator().toString())) {
            this.oprt.put(node.getOperator().toString(), 1);
        } else {
            this.oprt.put(node.getOperator().toString(), this.oprt.get(node.getOperator().toString()) + 1);
        }

        return true;
    }


    // Override visit the Assignment statements nodes.
    // if the assignment's operator doesn't exist in the operator hashmap, insert it, otherwise, increment the count field.
    public boolean visit(Assignment node) {
        if (!this.oprt.containsKey(node.getOperator().toString())) {
            this.oprt.put(node.getOperator().toString(), 1);
        } else {
            this.oprt.put(node.getOperator().toString(), this.oprt.get(node.getOperator().toString()) + 1);
        }

        return true;
    }


    // Override visit the Single Variable Declaration nodes.
    // add the "=" operators to the hashmap of operators if the variable is initialized
    public boolean visit(SingleVariableDeclaration node) {
        if (node.getInitializer() != null) {
            if (!this.oprt.containsKey("=")) {
                this.oprt.put("=", 1);
            } else {
                this.oprt.put("=", this.oprt.get("=") + 1);
            }
        }

        return true;
    }


    // Override visit the Variable Declaration Fragment nodes.
    // add the "=" operators to the hashmap of operators if the variable is initialized
    public boolean visit(VariableDeclarationFragment node) {

        if (node.getInitializer() != null) {
            if (!this.oprt.containsKey("=")) {
                this.oprt.put("=", 1);
            } else {
                this.oprt.put("=", this.oprt.get("=") + 1);
            }
        }

        return true;
    }


    // Override visit the SimpleNames nodes.
    // if the SimpleName doesn't exist in the names hashmap, insert it, otherwise, increment the count field.
    public boolean visit(SimpleName node) {
        if (!this.names.containsKey(node.getIdentifier())) {
            this.names.put(node.getIdentifier(), 1);
        } else {
            this.names.put(node.getIdentifier(), this.names.get(node.getIdentifier()) + 1);
        }
        return true;
    }


    // Override visit the null nodes.
    // if the null doesn't exist in the names hashmap, insert it, otherwise, increment the count field.
    public boolean visit(NullLiteral node) {
        if (!this.names.containsKey("null")) {
            this.names.put("null", 1);
        } else {
            this.names.put("null", this.names.get("null") + 1);
        }

        return true;
    }


    // Override visit the string literal nodes.
    // if the string literal doesn't exist in the names hashmap, insert it, otherwise, increment the count field.
    public boolean visit(StringLiteral node) {

        if (!this.names.containsKey(node.getLiteralValue())) {
            this.names.put(node.getLiteralValue(), 1);
        } else {
            this.names.put(node.getLiteralValue(), this.names.get(node.getLiteralValue()) + 1);
        }
        return true;
    }


    // Override visit the character literal nodes.
    // if the character literal doesn't exist in the names hashmap, insert it, otherwise, increment the count field.
    public boolean visit(CharacterLiteral node) {

        if (!this.names.containsKey(Character.toString(node.charValue()))) {
            this.names.put(Character.toString(node.charValue()), 1);
        } else {
            this.names.put(Character.toString(node.charValue()), this.names.get(Character.toString(node.charValue())) + 1);
        }

        return true;
    }


    // Override visit the boolean literal nodes.
    // if the boolean literal doesn't exist in the names hashmap, insert it, otherwise, increment the count field.
    public boolean visit(BooleanLiteral node) {

        if (!this.names.containsKey(Boolean.toString(node.booleanValue()))) {
            this.names.put(Boolean.toString(node.booleanValue()), 1);
        } else {
            this.names.put(Boolean.toString(node.booleanValue()), this.names.get(Boolean.toString(node.booleanValue())) + 1);
        }


        return true;
    }


    // Override visit the Number literal nodes.
    // if the Number literal doesn't exist in the names hashmap, insert it, otherwise, increment the count field.
    public boolean visit(NumberLiteral node) {
        if (!this.names.containsKey(node.getToken())) {
            this.names.put(node.getToken(), 1);
        } else {
            this.names.put(node.getToken(), this.names.get(node.getToken()) + 1);
        }

        return true;
    }


    // Override visit the compilationUnit to be able to retrieve the line numbers.
    public boolean visit(CompilationUnit unit) {
        compilation = unit;
        return true;
    }
}
